#include "ANN.h"

#include <random>
#include <chrono>


ANN::ANN( int inputs, int hiddenLayers, int nodesPerHiddenLayer, int outputs, float learningRateInput, float errorThresholdInput ){
    learningRate = learningRateInput;
    errorThreshold = errorThresholdInput;
    layers = hiddenLayers+2;                        //Input + hidden + output layers
    std::minstd_rand0 generator (std::chrono::system_clock::now().time_since_epoch().count());
    //std::minstd_rand0 generator (1337);
    net.push_back(NodeCluster( inputs, nodesPerHiddenLayer, generator ));
    for ( int i = 0; i < hiddenLayers; i++ ) {
        net.push_back(NodeCluster( nodesPerHiddenLayer, nodesPerHiddenLayer, generator ));
    }
    net.push_back(NodeCluster( nodesPerHiddenLayer, outputs, generator ));
    result = NULL;
}

ANN::ANN( std::ifstream & weightFile ){
    //learningRate = learningRateInput;
    weightFile.ignore(256,' ');
    weightFile >> layers;
    weightFile.ignore(256,' ');
    weightFile >> learningRate;
    weightFile.ignore(256,' ');
    weightFile >> errorThreshold;
    for ( int i = 0; i < layers; i++ ) {
        net.push_back(NodeCluster( weightFile ));
    }
    //this->loadWeights( weightFile );
    result = NULL;
}

ANN::~ANN() {
    //dtor
}

void ANN::saveWeights( std::ofstream & weightFile ) {

    weightFile << "Layers: " << layers << "\n";
    weightFile << "LearningRate: " << learningRate << "\n";
    weightFile << "ErrorThreshold: " << errorThreshold << "\n";
    for ( std::vector<NodeCluster>::iterator cluster = net.begin() ; cluster != net.end() ; cluster++ ) {
        cluster->saveClusterWeights( weightFile );
    }
}

void ANN::loadWeights( std::ifstream & weightFile ) {

    for ( std::vector<NodeCluster>::iterator cluster = net.begin() ; cluster != net.end() ; cluster++ ) {
        cluster->loadClusterWeights( weightFile );
    }
}

void ANN::teach( std::vector<float> * input, std::vector<float> * expectedOutput ){
    delete result;
    result = this->calculate( input );
    std::vector<float> * errData;
    errData = new std::vector<float>( 0,0.f);
    float error;
    for ( int i = 0; i < expectedOutput->size(); i++ ){
        error = ((*expectedOutput)[i] - (*result)[i]);
        if ( fabs( error ) < errorThreshold ) {
            //std::cout << (*result)[i] << " " << fabs(error) << " " << "\n";
            error = 0.0f;

        } else {
            error *= (*result)[i]*(1.0f - (*result)[i]);
        }
        errData->push_back(error);
        //errData->push_back( (((*expectedOutput)[i] - (*result)[i])*(*result)[i]*(1.0f - (*result)[i])) );
    }
    for ( int i = layers-1; i >= 0; i-- ) {
        errData = net[i].getUpstreamErrors(errData);
        net[i].updateWeights( learningRate );
    }
    delete errData;
}

std::vector<float> * ANN::calculate( std::vector<float> * input ){
    std::vector<float> * otpData;

    otpData = net[0].computeOutput(input);
    for ( int i = 1; i < layers; i++ ) {
        otpData = net[i].computeOutput(otpData);
    }
    return otpData;
}

void ANN::printWeights() {
    std::cout << "\n\nWeights\n";
    for ( int i = 0; i < layers; i++ ) {
        std::cout << "Layer "<< i <<":\n";
        net[i].printWeights();
    }
}

int ANN::getLayers() {
    return layers;
}
