#include "NodeCluster.h"

#include <stdlib.h>
#include <math.h>
#include <random>
#include <string>


Node::Node( int inputSize, std::minstd_rand0 & generator ) {
    weights.resize(inputSize,0.3f);
    for ( int i = 0 ; i < weights.size() ; i++ ) {
        weights[i] = generator() / ( generator.max()*0.5f ) - 1.0f;
    }
}

Node::Node( int inputSize, std::ifstream & weightFile ) {
    weights.resize(inputSize,0.3f);
    this->loadNodeWeights( weightFile );
}

Node::~Node() {

}

float Node::compute( std::vector<float> * input ) {

    unsigned int i = 0;
    double output = 0;

    std::vector<float>::iterator inp = input->begin();
    while ( inp != input->end() && i < weights.size() ) {
        output += this->weights[i] * (*inp);
        inp++;
        i++;
    }

    output = 1/( 1 + exp(-output));
    return output;
}

void Node::trainNode( std::vector<float> * input, const float error, float learningRate  ) {
    unsigned int i = 0;
    std::vector<float>::iterator inp = input->begin();
    while ( inp != input->end() && i < weights.size() ) {
        weights[i] += (learningRate * (*inp) * error);
        inp++;
        i++;
    }
}

void Node::saveNodeWeights ( std::ofstream & weightFile ) {
    for ( std::vector<double>::iterator it = weights.begin( ) ; it != weights.end() ; it++ ) {
        weightFile << *it << " ";
    }
    weightFile << "\n";
}

void Node::loadNodeWeights ( std::ifstream & weightFile ) {
    for ( std::vector<double>::iterator it = weights.begin( ) ; it != weights.end() ; it++ ) {
        weightFile >> *it;
        //std::cout << *it << " ";
    }
    //std::cout << "\n";
}

NodeCluster::NodeCluster( int inputSize, int outputSize, std::minstd_rand0 & generator ) {
    //nodes.resize(outputSize,Node(inputSize+1, generator));
    for ( int i = 0; i < outputSize; i++ ) {
        nodes.push_back( Node(inputSize+1, generator) );
    }
    input = nullptr;
    error = nullptr;
}

NodeCluster::NodeCluster( std::ifstream & weightFile ) {
    int inputSize;
    int nodesSize;
    std::string discard;
    weightFile >> discard;
    //weightFile.ignore(256,' ');
    weightFile >> inputSize;

    //weightFile.ignore(256,' ');
    weightFile >> discard;
    weightFile >> nodesSize;

    for ( int i = 0; i < nodesSize; i++ ) {
        nodes.push_back( Node(inputSize, weightFile) );
    }

    input = nullptr;
    error = nullptr;
}

NodeCluster::~NodeCluster() {
    //dtor
}

void NodeCluster::updateWeights( float learningRate ) {
    std::vector<float>::iterator ers = error->begin();
    for ( std::vector<Node>::iterator node = nodes.begin() ; node != nodes.end() ; node++ ) {
        node->trainNode( input, *ers, learningRate );
        ers++;
    }
}

void NodeCluster::saveClusterWeights ( std::ofstream & weightFile ) {
    weightFile << "InputWeights: " << (nodes.begin())->weights.size() << "\nNodes: " << nodes.size() << "\n";
    for ( std::vector<Node>::iterator node = nodes.begin() ; node != nodes.end() ; node++ ) {
        node->saveNodeWeights ( weightFile );
    }
}

void NodeCluster::loadClusterWeights ( std::ifstream & weightFile ) {
    for ( std::vector<Node>::iterator node = nodes.begin() ; node != nodes.end() ; node++ ) {
        node->loadNodeWeights ( weightFile );
    }
}

void NodeCluster::printWeights(){
    for ( std::vector<Node>::iterator node = nodes.begin() ; node != nodes.end() ; node++ ) {
        for ( int i = 0; i < node->weights.size(); i++ ) {
            std::cout << node->weights[i] << " ";
        }
        std::cout << "\n";
    }
}

std::vector<float> * NodeCluster::computeOutput( std::vector<float> * inputData ) {
    delete input;
    input = inputData;
    input->push_back(1.0f);
    std::vector<float> * output = new std::vector<float>( nodes.size(),0.f);
    std::vector<float>::iterator otp = output->begin();
    for ( std::vector<Node>::iterator node = nodes.begin() ; node != nodes.end() ; node++ ) {
        *otp = node->compute( input );
        otp++;
    }

    return output;
}

std::vector<float> * NodeCluster::getUpstreamErrors( std::vector<float> * errorData ) {
    delete error;
    error = errorData;
    std::vector<float> * upstreamError = new std::vector<float>( input->size(),0.f);

    for ( int i = 0 ; i < upstreamError->size() ; i++ ) {
        for ( int j = 0 ; j < nodes.size() ; j++ ) {
            (*upstreamError)[i] += (*error)[j] * nodes[j].weights[i];
        }
        (*upstreamError)[i] *= (*input)[i]*(1.f - (*input)[i]);
    }
    return upstreamError;
}
