#include <iomanip>
#include <vector>

#include "NodeCluster.h"
#include "CharacterRecognition.h"

int levels = 1;
int inNodes = 36;
float learningRate = 0.1f;
float errorThreshold = 0.3f;
int runs = 750;
bool load = false;

void menu() {
	int input;
	do
	{
		//levels, internal nodes, learning rate, error treshold, runs
		std::cout	<< std::endl << std::endl << std::endl << std::endl << std::endl << std::endl
					<< "Do you whish to edit data?" << std::endl
					<< std::setw(5) << '1' << std::setw(25) << "Hidden layers = " << std::setw(10) << levels << std::endl
					<< std::setw(5) << '2' << std::setw(25) << "Internal nodes = " << std::setw(10) << inNodes << std::endl
					<< std::setw(5) << '3' << std::setw(25) << "Learning rate = " << std::setw(10) << learningRate << std::endl
					<< std::setw(5) << '4' << std::setw(25) << "Error Threshold = " << std::setw(10) << errorThreshold << std::endl
					<< std::setw(5) << '5' << std::setw(25) << "Runs = " << std::setw(10) << runs << std::endl
					<< std::setw(5) << '9' << std::setw(25) << "Run program with wheights from wheights.txt" << std::endl
					<< std::setw(5) << '0' << std::setw(25) << "Run program." << std::endl
					<< std::endl << "choice: ";

		std::cin >> input;
		switch (input)
		{
		case 1:
			std::cout << "Set levels to: ";
			std::cin >> levels;
			break;
		case 2:
			std::cout << "Set input nodes to: ";
			std::cin >> inNodes;
			break;
		case 3:
			std::cout << "Set learning rate to: ";
			std::cin >> learningRate;
			break;
		case 4:
			std::cout << "Set error threshold to: ";
			std::cin >> errorThreshold;
			break;
		case 5:
			std::cout << "Set runs to: ";
			std::cin >> runs;
			break;
		case 9:
			load = true;
			input = 0;
		case 0:
			std::cout << std::endl << std::endl;
			break;
		default:
			std::cout << "invalid output";
			break;
		}

	} while (input != 0);
}

int main() {

	menu();

	CharacterRecognition * recog;
	if (load)
	{
		recog = new CharacterRecognition("weights.txt");
	}
	else
	{
		recog = new CharacterRecognition(levels, inNodes, learningRate, errorThreshold);
	}
    
    std::ofstream logFile;
    logFile.open( "log.txt" );
    logFile << "Run\n";
    logFile.close();


	for (int i = 0; i < runs; i++) {
		if (i % 10 == 0) std::cout << "Run " << i << " of " << runs << " runs." << std::endl;
		if (i % 100 == 0)
		{
			std::cout << std::endl << std::endl;
			recog->test(11, 20);
			std::cout << std::endl << std::endl;
		}
        recog->train();
        recog->log( 11, 20 , "log.txt");
    }

    //recog.test(1,10);
    recog->test(11,20);
    //recog.print();

	//stand still
	char bladi;
	std::cin >> bladi;

    return 0;
}
