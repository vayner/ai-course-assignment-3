#include "CharacterRecognition.h"


CharacterData::CharacterData( std::string textureName, char characterData ) {

    character = characterData;
    sf::Image image;

    imageData = new std::vector<float>();

    if (!image.loadFromFile(std::string( "textures/" + textureName ))){
        std::cout << "Error\n";
    } else {

        for ( int i = 0 ; i < 6 ; i++ ) {
            for ( int j = 0 ; j < 6 ; j++ ) {
                float data = 0.0f;
                for ( int x = i*5 ; x < i*5 + 5 ; x++ ) {
                    for ( int y = j*5 ; y < j*5 + 5 ; y++ ) {
                        data += image.getPixel(x,y).g;
                    }
                }
                data *= 1/(255.0f*25);
                imageData->push_back( data );
            }
        }
    }
}

CharacterData::~CharacterData() {

}

CharacterRecognition::CharacterRecognition( int hiddenLayers, int nodesPerHiddenLayer, float learningRate, float errorThreshold ) {    //pixelDensity
    generateCharacterData();
    net = new ANN( 36, hiddenLayers, nodesPerHiddenLayer, 26, learningRate, errorThreshold );
}

CharacterRecognition::CharacterRecognition( std::string weightFileName ) {

    std::ifstream weightFile;

    weightFile.open( weightFileName );
    net = new ANN( weightFile );
    //net->loadWeights( weightFile );
    weightFile.close();

    generateCharacterData();

}

CharacterRecognition::~CharacterRecognition() {

    std::ofstream weightFile;
    weightFile.open( "weights.txt" );
    //weightFile << net->getLayers() << "\n";
    net->saveWeights( weightFile );
    weightFile.close();

    delete net;
}

void CharacterRecognition::generateCharacterData() {
    //layers = hiddenLayers + 2;
    std::string imageName;
    for ( int i = 65 ; i <= 90 ; i++ ) {
        for ( int j = 1 ; j <= 10 ; j++ ) {
            imageName = char(i) + std::to_string(j) + ".jpg";
            charData.push_back(CharacterData(imageName, char(i)));
        }
    }
}

void CharacterRecognition::train() {

    std::vector<float> * inputData;
    std::vector<float> * expectedResult;

    for ( std::vector<CharacterData>::iterator it = charData.begin() ; it != charData.end() ; it++  ){
        expectedResult = new std::vector<float>(26, 0.0f);
        (*expectedResult)[it->character-65] = 1.0f;
        inputData = new std::vector<float>( *it->imageData );
        net->teach( inputData, expectedResult);
    }
}

void CharacterRecognition::log( int start, int stop, std::string logFileName ) {
    std::ofstream logFile;
    logFile.open( logFileName, std::ofstream::out | std::ofstream::app );
    std::string imageName;
    float total = 0;
    float percent;
    for ( int i = 65 ; i <= 90 ; i++ ) {
        percent = 0;
        for ( int j = start ; j <= stop ; j++ ) {
            imageName = char(i) + std::to_string(j) + ".jpg";
            if( this->recognize( imageName )  == char(i) )  {
                percent += 10;
            }
        }

        total += percent;

    }
    total /= 26;
    logFile << std::showpoint << int(total*100.0f) << "\n";
    logFile.close();
}

void CharacterRecognition::test( int startIndex, int endIndex ){
    float total = 0;
    float percent;
    char letter;
    std::string imageName;
    for ( int i = 65 ; i <= 90 ; i++ ) {
        std::cout << char(i) << ": ";
        percent = 0;

        for ( int j = startIndex ; j <= endIndex ; j++ ) {
            imageName = char(i) + std::to_string(j) + ".jpg";
            letter = this->recognize( imageName );
            std::cout << letter;
            if(  letter == char(i) )  {
                percent += 100/( 1 + endIndex - startIndex );
            }
        }

        total += percent;
        std::cout << "\t" << percent << "\n";

    }
    total /= 26;
    std::cout << "total: " << total << "\n";
}

char CharacterRecognition::recognize( std::string textureName ) {
    CharacterData * testData = new CharacterData( textureName, '*' );
    std::vector<float> * inputData;
    std::vector<float> * result;

    inputData = new std::vector<float>( *testData->imageData );
    result = net->calculate( inputData );
    int indexMax = 0;
    for( int i = 0 ; i <= 25 ; i++ ) {
        if ( (*result)[i] > (*result)[indexMax] ) {
            indexMax = i;
        }
    }
    delete result;

    return char(indexMax+65);
}

void CharacterRecognition::print(){
    for ( int i = 0; i < net->result->size(); i++ ) {
        std::cout << (*(net->result))[i] << "\n";
    }
    std::cout << "\n";
}

