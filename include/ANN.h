#ifndef ANN_H
#define ANN_H

#include <fstream>
#include <vector>

#include "NodeCluster.h"

const int N = 1;

class ANN
{
    public:
        /** Default constructor */
        ANN( int inputs, int hiddenLayers, int nodesPerHiddenLayer, int outputs, float learningRateInput, float errorThresholdInput );
        ANN( std::ifstream & weightFile );
        /** Default destructor */
        virtual ~ANN();
        std::vector<float> * calculate( std::vector<float> * input );
        void teach( std::vector<float> * input, std::vector<float> * expectedOutput );
        void printWeights();
        void saveWeights( std::ofstream & weightFile );
        void loadWeights( std::ifstream & weightFile );
        int getLayers();

        std::vector<float> * result;
    protected:
        std::vector<NodeCluster> net;
        int layers;
        float learningRate;
        float errorThreshold;

    private:
};

#endif // ANN_H
