#ifndef NODECLUSTER_H
#define NODECLUSTER_H

#include <vector>
#include <iostream>
#include <random>
#include <fstream>

class NodeCluster;

class Node {
    friend class NodeCluster;
    public:
        Node( int inputSize, std::minstd_rand0 & generator );
        Node( int inputSize, std::ifstream & weightFile );
        virtual ~Node();
        float compute( std::vector<float> * input );
        void trainNode( std::vector<float> * input, const float error, float learningRate );

        void saveNodeWeights ( std::ofstream & weightFile );
        void loadNodeWeights ( std::ifstream & weightFile );

    protected:
        //float computeError( const float output, std::vector<float> * downstreamOutputErrors  );

        std::vector<double> weights;
    private:
};

class NodeCluster {

    public:
        NodeCluster( int inputSize, int outputSize, std::minstd_rand0 & generator );
        NodeCluster( std::ifstream & weightFile );
        virtual ~NodeCluster();
        void updateWeights( float learningRate );
        std::vector<float> * computeOutput( std::vector<float> * input );
        std::vector<float> * getUpstreamErrors( std::vector<float> * errorData );
        void printWeights();

        void saveClusterWeights ( std::ofstream & weightFile );
        void loadClusterWeights ( std::ifstream & weightFile );

    protected:
    private:
        std::vector<Node> nodes;
        std::vector<float> * input;
        std::vector<float> * output;
        std::vector<float> * error;

};

#endif // NODECLUSTER_H
