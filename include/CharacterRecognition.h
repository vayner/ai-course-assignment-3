#ifndef CHARACTERRECOGNITION_H
#define CHARACTERRECOGNITION_H

#include <string>
#include <iostream>
#include <vector>

#include <SFML\Graphics\Image.hpp>

#include "ANN.h"

class CharacterData {
    friend class CharacterRecognition;
    public:
        CharacterData( std::string textureName, char character );
        virtual ~CharacterData();
    protected:
        std::vector<float> * imageData;
        char character;
};

class CharacterRecognition{
    public:
        CharacterRecognition( int hiddenLayers, int nodesPerHiddenLayer, float learningRate, float errorThreshold );
        CharacterRecognition( std::string weightFileName );
        virtual ~CharacterRecognition();
        void generateCharacterData();
        void train();
        void test(  int startIndex, int endIndex );
        char recognize( std::string textureName );
        void print();
        void log( int start, int stop, std::string logFileName );
    protected:
        std::vector<CharacterData> charData;
        ANN * net;
        int layers;
    private:
};

#endif // CHARACTERRECOGNITION_H
